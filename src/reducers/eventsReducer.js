const initialState = {
    detail: [], 
    event: []
}
export default(state = initialState, action) => {
    switch(action.type) {
        case 'EVENT_TYPE':
            return {
                ...state, 
                event: action.payload
            }
            case 'EVENT_DETAIL':
            return {
                ...state, 
                detail: action.payload
            }
        case 'EVENT_DETAIL_LOADING':
            return {
                ...state,
                loding: action.payload
            }
        default: 
            return state
    }
}