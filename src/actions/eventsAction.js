import axios from 'axios';

const URL = 'https://cors-anywhere.herokuapp.com/https://api.smarkets.com/v3/popular/event_ids/'
const URL2 = 'https://api.smarkets.com/v3/events'

export const EVENT_TYPE = 'EVENT_TYPE';
export const EVENT_DETAIL = 'EVENT_DETAIL';

export const eventType = (payload) => {
    return {
        type: EVENT_TYPE,
        payload 
    };
}

export const eventDetail = (payload) => {
    return {
        type: EVENT_DETAIL,
        data: payload.data,
        index: payload.index
    };
}

export const getData = (events) =>  {
    return function(dispatch) {
    const url = `${URL}`;
    axios.get(url)
        .then (function (response) {
           dispatch(eventType());
           console.log(response.data)
        })
        .catch(function(error) {
            console.log(error);
        })
    }
}

export const getDetail = (events, index) =>  {
    return function(dispatch) {
    const url = `${URL2}/${events}`;
    axios.get(url)
        .then (function (response) {
           dispatch(eventDetail({
               data: response.data.events,
               index: index
           }))
           console.log('api 2' + response.data)
        })
        .catch(function(error) {
            console.log(error);
        })
    }
}