import React, {Component} from 'react';
import Home from './Home'
import './App.css';
import styled from '@emotion/styled'
import { css } from '@emotion/core'


 const background = (props) => css`
 background: black;
 min-height: 100vh;

`

const PageContainer = styled('div')`
 ${background};
`

class App extends Component {
 
   render() {
  return (
    <PageContainer>
    <div>
      <header >
        <Home />
      </header>
    </div>
    </PageContainer>
  );
}
}

export default App;
