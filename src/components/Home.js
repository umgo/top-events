import React from 'react';
import Header from './Header'
import Events from './Events'
import EventDetails from './EventDetails'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'

function Home() {
  return (
    <Container>
        <Row>
            <Col>
                <Header />
            </Col>
        </Row>
        <Row>
            <Col md={6}>
                <Events />
            </Col>
            <Col md={6}>
                <EventDetails />
            </Col>
        </Row>
    </Container>
  );
}

export default Home;
