import React, {Component} from 'react';
import { getData, getDetail } from '../actions/eventsAction';
import { connect } from 'react-redux';
import styled from '@emotion/styled'

const mapStateToProps = state => ({
    ...state
   })

const mapDispatchToProps = dispatch => ({
    getData: () => dispatch(getData()),
    getDetail: () => dispatch(getDetail())
})

const Event = styled.div`
  background: #151515;
  padding: 2px;
  border: 1px #151515 solid;
  border-radius: 10px;
  width: 100%;
  color:white;
`
   
class Events extends Component {
    componentWillMount= () =>  {
        this.props.getData();
       }
       render(){
  return (
      <Event>
     <p>Events</p>
     </Event>
     
  );
       }
}

export default connect(mapStateToProps, mapDispatchToProps)(Events);
