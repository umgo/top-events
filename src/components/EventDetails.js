import React, {Component} from 'react';
import { getDetail } from '../actions/eventsAction';
import { connect } from 'react-redux';
import styled from '@emotion/styled'

const mapStateToProps = state => ({
    ...state
   })

const mapDispatchToProps = dispatch => ({
  getDetail: () => dispatch(getDetail())
})

const EventDetail = styled.div`
  background: #151515;
  padding: 2px;
  border: 1px #151515 solid;
  border-radius: 10px;
  width: 100%;
  color:white;
`
   
class EventDetails extends Component {
  componentWillMount = () =>  {
    this.props.getDetail();
    console.log(this.props.getDetail())
   }
       render(){
  return (
      <EventDetail>
     <p>Event Details</p>
     </EventDetail>
  );
       }
}

export default connect(mapStateToProps, mapDispatchToProps)(EventDetails);
