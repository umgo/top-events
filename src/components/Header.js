import React from 'react'
import styled from '@emotion/styled'
import logo from '../images/smarkets-logo.svg'
const NavBar = styled.div`
  background: #0C0C0C;
  width:100vw;
  margin-left:-15%;
`

function Header() {
  return (
    <NavBar>
     <img src={logo}/>
    </NavBar>
  );
}

export default Header;
