# Top Events 
* An app that shows top events and details of those events 

## Technology used 
* React
* Redux 
* EmotionJS

## To Run the App
* yarn
* yarn start
* localhost: 3000

## Successes
* In-line styling (EmotionJS) 
* App renders correctly 

## Challenges 
* Couldn't get Redux to dispatch the information about each top event 
* No testing or further development because of road block with Redux 

## Things I would do differently next time 
* Perhaps creating the app without Redux to reduce complexity 
* Added testing and uploaded to Heroku